<?php

/**
 * @file
 * API documentation for the Discourse SSO module.
 */

/**
 * Alter the parameters for the Discourse SSO.
 *
 * @param array $parameters
 *   The parameters to be altered.
 *
 * @see \Drupal\discourse_sso\Controller\DiscourseSsoController::validate
 */
function hook_discourse_sso_parameters_alter(array &$parameters) {
  $parameters['bio'] = 'User bio';
}
