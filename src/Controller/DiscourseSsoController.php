<?php

namespace Drupal\discourse_sso\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Discourse SSO controller.
 */
class DiscourseSsoController extends ControllerBase {

  protected $payload;

  protected $sig;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * DiscourseSsoController constructor.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $config
   */
  public function __construct(ImmutableConfig $config) {
    $this->config = $config;
    $this->payload = $_GET['sso'] ?? NULL;
    $this->sig = $_GET['sig'] ?? NULL;
    if (!$this->payload) {
      $this->payload = $_SESSION['discourse_sso_payload'] ?? NULL;
    }
    if (!$this->sig) {
      $this->sig = $_SESSION['discourse_sso_sig'] ?? NULL;
    }
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): DiscourseSsoController {
    /** @noinspection NullPointerExceptionInspection */
    return new static(
      $container->get('config.factory')->get('discourse_sso.settings')
    );
  }

  /**
   * @return \Drupal\Core\Access\AccessResult
   */
  public function access(): AccessResult {
    if (!$this->payload || !$this->sig) {
      // This will do a redirect.
      return AccessResult::allowed();
    }
    if ($this->currentUser()->id()) {
      $payload = urldecode($this->payload);
      if (hash_hmac('sha256', $payload, $this->config->get('discourse_sso_secret')) !== $this->sig) {
        return AccessResult::forbidden();
      }
      parse_str(base64_decode($payload), $query);
      if (!isset($query['nonce'])) {
        return AccessResult::forbidden();
      }
    }
    return AccessResult::allowed();
  }

  /**
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function discourse_sso(): RedirectResponse {
    if (!$this->payload || !$this->sig) {
      return $this->retry();
    }
    if ($this->currentUser()->id()) {
      return $this->validate();
    }
    return $this->login();
  }

  /**
   * Function is called if a user is authenticated with the primary Drupal
   * website.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   */
  protected function validate(): TrustedRedirectResponse {
    $payload = urldecode($this->payload);
    $query = [];
    parse_str(base64_decode($payload), $query);
    /** @var \Drupal\user\UserInterface $account */
    $account = NULL;
    try {
      $account = $this->entityTypeManager()->getStorage('user')
        ->load($this->currentUser()->id());
    } catch (InvalidPluginDefinitionException $e) {
      // Can be ignored.
    } catch (PluginNotFoundException $e) {
      // Can be ignored.
    }
    if ($account === NULL) {
      return $this->retry();
    }
    $picture = '';
    if ($account->hasField('user_picture') && !$account->get('user_picture')->isEmpty()) {
      $picture = \Drupal::service('file_url_generator')->generateAbsoluteString($account->get('user_picture')->entity->getFileUri());
    }

    // Create the payload
    $real_name_field = $this->config->get('user_real_name_field');
    $real_name = (!empty($real_name_field) && isset($account->{$real_name_field})) ?
      $account->{$real_name_field}->value : '';
    $discourse_username_field = $this->config->get('user_discourse_username_field');
    $username = (!empty($discourse_username_field) && isset($account->{$discourse_username_field}))
      ? $account->{$discourse_username_field}->value
      : $account->getAccountName();
    $parameters = [
      'username' => $username,
      'external_id' => $account->id(),
      'name' => $real_name,
      'email' => $account->getEmail(),
      'avatar_force_update' => TRUE,
      'avatar_url' => $picture,
      'nonce' => $query['nonce'],
    ];
    $this->moduleHandler()->alter('discourse_sso_parameters', $parameters);
    $return_payload = base64_encode(http_build_query($parameters));
    $return_sig = hash_hmac('sha256', $return_payload, $this->config->get('discourse_sso_secret'));
    $response = new TrustedRedirectResponse($this->config->get('discourse_server') . '/session/sso_login?sso=' . $return_payload . '&sig=' . $return_sig);
    $response->getCacheableMetadata()->setCacheMaxAge(0);
    return $response;
  }

  /**
   * Function is called if a user is not authenticated with the primary Drupal
   * website.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  protected function login(): RedirectResponse {
    $_SESSION['discourse_sso_sig'] = $this->sig;
    $_SESSION['discourse_sso_payload'] = $this->payload;

    $options = [
      'query' => ['destination' => 'discourse_sso?sso=' . $this->payload . '&sig=' . $this->sig],
      'absolute' => TRUE,
    ];
    return $this->redirect('user.login', [], $options);
  }

  /**
   * Function is called if either payload or sig is not set.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   */
  protected function retry(): TrustedRedirectResponse {
    $response = new TrustedRedirectResponse($this->config->get('discourse_server') . '/session/sso');
    $response->getCacheableMetadata()->setCacheMaxAge(0);
    return $response;
  }

}
