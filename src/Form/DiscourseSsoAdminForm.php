<?php

namespace Drupal\discourse_sso\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DiscourseSsoAdminForm extends ConfigFormBase {

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  private $entityFieldManager;

  /**
   * DiscourseSsoAdminForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($config_factory);

    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): DiscourseSsoAdminForm {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'discourse_sso_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['discourse_sso.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('discourse_sso.settings');

    $form['discourse_server'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Discourse URL'),
      '#description' => $this->t('The web address of the Discourse server.'),
      '#size' => 40,
      '#maxlength' => 120,
      '#required' => TRUE,
      '#default_value' => $config->get('discourse_server'),
    ];

    $form['discourse_sso_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SSO Secret'),
      '#description' => $this->t('Secret string used to encrypt/decrypt SSO information, be sure it is 10 chars or longer and matches the Discourse setting under Admin > Settings > Login. If there is a value here Drupal will attempt to login the user to Discourse on Drupal login.'),
      '#default_value' => $config->get('discourse_sso_secret'),
    ];

    $form['api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API user name'),
      '#description' => $this->t('The name of the discourse user.'),
      '#default_value' => $config->get('api_username'),
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('The API Key of the discourse user.'),
      '#default_value' => $config->get('api_key'),
    ];

    $options = $this->getTextFieldsByEntityTypeBundle('user', 'user');
    if (count($options)) {
      $form['user_discourse_username_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Alternative Discourse username field'),
        '#description' => $this->t('The Drupal user text field to use for the discourse system username field. This is useful if you have mail addresses as usernames and don\'t want discourse to create usernames from the email.'),
        '#default_value' => $config->get('user_discourse_username_field'),
        '#options' => $options,
        '#empty_value' => TRUE,
      ];
      $form['user_real_name_field'] = [
        '#type' => 'select',
        '#title' => $this->t('User real name field'),
        '#description' => $this->t('The Drupal user text field to use for the discourse user name field.'),
        '#default_value' => $config->get('user_real_name_field'),
        '#options' => $options,
        '#empty_value' => TRUE,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $url = $form_state->getValue(['discourse_server']);

    if (strpos($url, 'http') !== 0) {
      $form_state->setValue(['discourse_server'], 'http://' . $url);
    }
    // Remove any trailing slash.
    if (substr($form_state->getValue([
        'discourse_server',
      ]), -1) === '/') {
      $form_state->setValue(['discourse_server'], substr($form_state->getValue([
        'discourse_server',
      ]), 0, -1));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('discourse_sso.settings')
      ->set('discourse_server', $form_state->getValue(['discourse_server']))
      ->set('discourse_sso_secret', $form_state->getValue(['discourse_sso_secret']))
      ->set('api_username', $form_state->getValue('api_username'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('user_discourse_username_field', $form_state->getValue('user_discourse_username_field'))
      ->set('user_real_name_field', $form_state->getValue('user_real_name_field'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * @param $entity_type
   * @param $bundle
   *
   * @return array
   */
  protected function getTextFieldsByEntityTypeBundle($entity_type, $bundle): array {
    $bundleFields = [];

    $fields = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    foreach ($fields as $field_name => $field_definition) {
      if (!empty($field_definition->getTargetBundle()) && $field_definition->getType() === 'string') {
        $bundleFields[$field_name] = $field_definition->getLabel();
      }
    }

    return $bundleFields;
  }

}
