<?php

namespace Drupal\discourse_sso;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\RequestOptions;

/**
 * Discourse single sign on base class.
 */
class SingleSignOnBase {

  use StringTranslationTrait;

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * @var bool
   *
   * @see \GuzzleHttp\RequestOptions::HTTP_ERRORS
   */
  protected $http_errors = FALSE;
  protected $url;
  protected $api_username;
  protected $api_key;

  /**
   * SingleSignOnBase constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   */
  public function __construct(ClientInterface $http_client, ConfigFactory $config_factory) {
    $this->client = $http_client;

    $config = $config_factory->get('discourse_sso.settings');
    $this->url = $config->get('discourse_server');
    $this->api_username = $config->get('api_username');
    $this->api_key = $config->get('api_key');
  }

  /**
   * @param null $name
   *
   * @return array|string|bool
   */
  protected function getDefaultParameter($name = NULL) {
    $parameters = [
      RequestOptions::QUERY => [
        'api_key' => $this->api_key,
        'api_username' => $this->api_username,
      ],
      RequestOptions::HEADERS => [
        'api-key' => $this->api_key,
        'api-username' => $this->api_username,
      ],
      RequestOptions::HTTP_ERRORS => $this->http_errors,
    ];

    if (isset($name)) {
      return $parameters[$name] ?? FALSE;
    }

    return $parameters;
  }
}
