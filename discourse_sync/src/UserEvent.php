<?php

namespace Drupal\discourse_sync;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event class to be dispatched from the discourse_sso controller.
 */
class UserEvent extends Event {

  public const EVENT = 'discourse_sync.user';

  /**
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * UserEvent constructor.
   *
   * @param \Drupal\user\Entity\User $user
   */
  public function __construct($user) {
    $this->user = $user;
  }

  /**
   * @return int
   */
  public function getUid(): int {
    return $this->user->id();
  }

  /**
   * @return string
   */
  public function getUsername(): string {
    return $this->user->getDisplayName();
  }

  /**
   * @return array
   */
  public function getUserRoles(): array {
    return $this->user->getRoles();
  }

}
