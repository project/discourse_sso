<?php

namespace Drupal\discourse_sync\EventSubscriber;

use Drupal\discourse_sync\Role;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\discourse_sync\UserEvent;

/**
 * EventSubscriber class
 */
class EventSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\discourse_sync\Role
   */
  protected $service;

  /**
   * EventSubscriber constructor.
   *
   * @param \Drupal\discourse_sync\Role $service
   */
  public function __construct(Role $service) {
    $this->service = $service;
  }


  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[UserEvent::EVENT][] = ['onUserCreate', 0];
    return $events;
  }

  /**
   * Handler for the discourse_sync user event.
   *
   * @param \Drupal\discourse_sync\UserEvent $event
   */
  public function onUserCreate(UserEvent $event): void {
    $this->service->syncUserRoles($event->getUsername(), $event->getUserRoles());
  }
}
