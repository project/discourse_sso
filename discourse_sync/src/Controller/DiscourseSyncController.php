<?php

namespace Drupal\discourse_sync\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\discourse_sync\UserEvent;
use Drupal\Core\Config\ConfigFactory;
use Symfony\Component\HttpFoundation\Request;

/**
 * Discourse Sync controller.
 */
class DiscourseSyncController extends ControllerBase {

  public const DISCOURSE_USER_CREATED_EVENT = 'user_created';

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * @var string
   */
  protected $webhookSecret;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * DiscourseSyncController constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   * @param \Drupal\Core\Config\ConfigFactory $config
   * @param \Symfony\Component\HttpFoundation\Request $request
   */
  public function __construct(EventDispatcherInterface $eventDispatcher,
    ConfigFactory $config, Request $request) {
    $this->eventDispatcher = $eventDispatcher;
    $this->webhookSecret = $config->get('discourse_sync.settings')->get('webhook_secret');
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('config.factory'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * @return array
   */
  public function userWebhook(): array {
    $discourse_event = $this->request->headers->get('x-discourse-event');
    if ($discourse_event !== self::DISCOURSE_USER_CREATED_EVENT) {
      return [];
    }

    $discourse_event_signature = $this->request->headers->get('x-discourse-event-signature');
    $discourse_payload_raw = file_get_contents('php://input');
    $signature = 'sha256=' . hash_hmac('sha256', $discourse_payload_raw, $this->webhookSecret);
    if ($signature !== $discourse_event_signature) {
      return [];
    }

    $payload = json_decode($discourse_payload_raw, TRUE);
    $username = $payload['user']['username'];
    $event = new UserEvent(user_load_by_name($username));
    $this->eventDispatcher->dispatch($event, UserEvent::EVENT);

    return [];
  }
}
