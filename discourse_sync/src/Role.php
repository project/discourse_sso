<?php

namespace Drupal\discourse_sync;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use \Drupal\discourse_sso\SingleSignOnBase;
use Drupal\user\Entity\User;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Synchronize Drupal roles to discourse using its json API.
 *
 * Discourse API doesn't get auth info from JSON data:
 * https://meta.discourse.org/t/json-vs-url-encoded-api-calls-json-returns-bad-csrf/22406
 * Hence add api_key and api_username to URL.
 */
class Role extends SingleSignOnBase {

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Role constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   * @param \Drupal\Core\Config\ConfigFactory $config
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   */
  public function __construct(ClientInterface $http_client, ConfigFactory $config, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger_channel_factory) {
    parent::__construct($http_client, $config);
    $this->messenger = $messenger;
    $this->logger = $logger_channel_factory->get('discourse_sync');
  }

  /**
   * @param $name
   * @param string $label
   * @param string $method
   *
   * @return array
   */
  public function createRole($name, $label = '', $method = 'POST'): array {
    $url = $this->url . '/admin/groups.json';
    $parameters = [
        'json' => [
          'group' => [
            'name' => $name,
            'full_name' => $label,
          ],
        ],
      ] + $this->getDefaultParameter();
    try {
      $request = $this->client->request($method, $url, $parameters);
      $response = json_decode($request->getBody(), TRUE);

      if (!empty($response)) {
        $id = $response['basic_group']['id'];
        $message = $this->t('Discourse group #@id: @name created.', [
          '@id' => $id,
          '@name' => $name,
        ]);
        $this->notify($message);
      }

      return $response;
    }
    catch (GuzzleException $e) {
      watchdog_exception('discourse_sync', $e, $e->getMessage());
    }
    return [];
  }

  /**
   * @param $name
   * @param string $label
   * @param string $method
   *
   * @return mixed
   */
  public function updateRole($name, $label = '', $method = 'PUT') {
    $id = $this->getGroupIdByName($name);
    if (!$id) {
      return $this->createRole($name, $label);
    }

    $url = $this->url . '/groups/' . $id . '.json';
    $parameters = [
        'json' => [
          'group' => [
            'full_name' => $label,
          ],
        ],
      ] + $this->getDefaultParameter();
    try {
      $request = $this->client->request($method, $url, $parameters);
      $response = json_decode($request->getBody(), TRUE);

      if (!empty($response) && isset($response['success'])) {
        $message = $this->t('Discourse group: @name updated.', ['@name' => $name]);
        $this->notify($message);
      }

      return $response;
    } catch (GuzzleException $e) {
      watchdog_exception('discourse_sync', $e, $e->getMessage());
    }
    return [];
  }

  /**
   * @param $name
   * @param string $method
   *
   * @return array
   */
  public function deleteRole($name, $method = 'DELETE'): array {
    $id = $this->getGroupIdByName($name);
    if (!$id) {
      $message = $this->t('Discourse not synchronized.');
      $message .= ' ' . $this->t('Deleted group @name does not exist.', ['@name' => $name]);
      $this->notify($message);
      return [];
    }

    $url = $this->url . '/admin/groups/' . $id . '.json';
    try {
      $request = $this->client->request($method, $url, $this->getDefaultParameter());
      if ($request->getStatusCode() === 200) {
        $response = json_decode($request->getBody(), TRUE);
        $message = $this->t('Discourse group #@id: @name deleted.', [
          '@id' => $id,
          '@name' => $name,
        ]);
        $this->notify($message);

        return $response;
      }
    }
    catch (GuzzleException $e) {
      watchdog_exception('discourse_sync', $e, $e->getMessage());
    }
    return [];
  }

  /**
   * @param $username
   * @param $roles
   */
  public function syncUserRoles($username, $roles): void {
    $user = $this->getUserByUsername($username);
    if (!$user) {
      $message = $this->t('Discourse not synchronized.');
      $message .= ' ' . $this->t('User @name does not exist.', ['@name' => $username]);
      $this->notify($message);
      return;
    }

    $usergroups = $this->getCustomUserGroups($user);

    // New roles
    $new = array_diff($roles, $usergroups);
    foreach ($new as $role) {
      $this->assignRole($username, $role);
    }

    // Removed roles
    $obsolete = array_diff($usergroups, $roles);
    foreach ($obsolete as $role) {
      $this->divestRole($username, $role);
    }
  }

  /**
   * @param $username
   * @param $role
   * @param string $method
   *
   * @return array
   */
  public function assignRole($username, $role, $method = 'PUT'): array {
    $gid = $this->getGroupIdByName($role);
    if (!$gid) {
      $message = $this->t('Discourse not synchronized.');
      $message .= ' ' . $this->t('Group @role does not exist.', ['@role' => $role]);
      $this->notify($message);
      return [];
    }

    $url = $this->url . '/groups/' . $gid . '/members.json';
    $parameters = [
        'json' => [
          'usernames' => $username,
        ],
      ] + $this->getDefaultParameter();
    try {
      $request = $this->client->request($method, $url, $parameters);
      $response = json_decode($request->getBody(), TRUE);

      if ($response && key($response) === 'success') {
        $message = $this->t('Discourse user @username: assigned to @role.', [
          '@username' => $username,
          '@role' => $role,
        ]);
        $this->notify($message);
      }

      return $response;
    }
    catch (GuzzleException $e) {
      watchdog_exception('discourse_sync', $e, $e->getMessage());
    }
    return [];
  }

  /**
   * @param $username
   * @param $role
   * @param string $method
   *
   * @return array
   */
  public function divestRole($username, $role, $method = 'DELETE'): array {
    $gid = $this->getGroupIdByName($role);
    if (!$gid) {
      $message = $this->t('Discourse not synchronized.');
      $message .= ' ' . $this->t('Group @role does not exist.', ['@role' => $role]);
      $this->notify($message);
      return [];
    }

    $user = $this->getUserByUsername($username);
    if (!$user) {
      $message = $this->t('Discourse not synchronized.');
      $message .= ' ' . $this->t('User @user does not exist.', ['@user' => $username]);
      $this->notify($message);
      return [];
    }

    $url = $this->url . '/groups/' . $gid . '/members.json';
    $parameter = [
        'json' => [
          'user_id' => $user['user']['id'],
        ],
      ] + $this->getDefaultParameter();
    try {
      $request = $this->client->request($method, $url, $parameter);
      if ($request->getStatusCode() === 200) {
        $response = json_decode($request->getBody(), TRUE);
        $message = $this->t('Discourse user @username: @role divested.', [
          '@username' => $username,
          '@role' => $role,
        ]);
        $this->notify($message);

        return $response;
      }
    }
    catch (GuzzleException $e) {
      watchdog_exception('discourse_sync', $e, $e->getMessage());
    }
    return [];
  }

  /**
   * Alter method for discourse_sync_discourse_sso_parameters_alter().
   *
   * Add groups to the sso payload, based on the user's roles.
   *
   * @see discourse_sync_discourse_sso_parameters_alter()
   */
  public function alterSsoParameters(&$parameters) {
    if (empty($parameters['external_id']) || !is_numeric($parameters['external_id'])) {
      return;
    }
    $account = User::load($parameters['external_id']);
    if (empty($account) || empty($account->getRoles(TRUE))) {
      return;
    }
    $parameters['add_groups'] = implode(',', $account->getRoles(TRUE));
  }

  /**
   * @param $name
   * @param string $method
   *
   * @return int|bool
   */
  protected function getGroupIdByName($name, $method = 'GET') {
    $url = $this->url . '/groups/search.json';
    try {
      $request = $this->client->request($method, $url, $this->getDefaultParameter());
      if ($request->getStatusCode() === 200) {
        $response = json_decode($request->getBody(), TRUE);

        foreach ($response as $group) {
          if ($group['name'] === $name) {
            return $group['id'];
          }
        }
      }
    }
    catch (GuzzleException $e) {
      watchdog_exception('discourse_sync', $e, $e->getMessage());
    }
    return FALSE;
  }

  /**
   * @param $name
   * @param string $method
   *
   * @return array
   */
  protected function getUserByUsername($name, $method = 'GET'): array {
    $result = [];
    $url = $this->url . '/users/' . $name . '.json';
    try {
      $request = $this->client->request($method, $url, $this->getDefaultParameter());
      if ($request->getStatusCode() === 200) {
        $result = json_decode($request->getBody(), TRUE) ?? [];
      }
    }
    catch (GuzzleException $e) {
      watchdog_exception('discourse_sync', $e, $e->getMessage());
    }
    return $result;
  }

  /**
   * @param $user
   *
   * @return array
   */
  protected function getCustomUserGroups($user): array {
    $groups = [];

    if (!empty($user['user']) && !empty($user['user']['groups'])) {
      foreach ($user['user']['groups'] as $group) {
        if ($group['automatic'] === FALSE) {
          $groups[$group['id']] = $group['name'];
        }
      }
    }

    return $groups;
  }

  /**
   * @param string $message
   */
  protected function notify($message): void {
    $this->logger->notice($message);
  }

}
